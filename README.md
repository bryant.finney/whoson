# Whoson Bot
Whoson is a Discord bot that allows users to set and get their own custom "walk-on" sound. This is the sound that plays when a user joins a voice channel in a Discord server.

## Usage
The bot supports the following commands:

### /whoson set {url} [start_time] [end_time]
Set the whoson sound for the current user.

- `url` is a required argument and can be either a direct audio link or a YouTube video link. If a YouTube link is provided, the bot will automatically trim the audio to 10 seconds.
- `start_time` is an optional argument and only applicable if a YouTube link is provided. It sets the start time of the audio in seconds.
- `end_time` is also an optional argument and only applicable if a YouTube link is provided. It sets the end time of the audio in seconds, with a maximum of `start_time + 10`.

### /whoson get
Get current whoson sound and post to text channel

### /whoson play
Play current whoson sound in voice channel.

### /whoson help
Get usage information about the bot.

## Installation
To install Whoson bot, you need to have Python and Poetry installed on your system.

### Install the Dependencies
This project uses python 3.10 and ffmpeg. If you are on ubuntu install these with the following command.

```bash
apt-get -y install python3.10 ffmpeg
```

### Install the Application

Install the dependencies with poetry:
```bash
poetry install
```

Copy the config.py.example file in the whoson directory of the project and add your Discord bot token to it:
```bash
cp whoson/config.py.example whoson/config.py
```

### Running the Bot
To run the bot, simply run the following command in the root directory of the project:

```bash
poetry run python whoson/main.py
```

## Contributing
We welcome contributions to Whoson bot! If you have any suggestions or find any bugs, please open an issue or submit a pull request.

Thank you for using Whoson bot!