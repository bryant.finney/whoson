from __future__ import unicode_literals

import asyncio
import os
import traceback
from typing import Optional, Union

from discord.channel import VoiceChannel, StageChannel
from pydub import AudioSegment
import requests
from glob import glob

from discord import (
    Member,
    File,
    VoiceState,
    FFmpegPCMAudio,
    VoiceClient,
    Guild,
    TextChannel,
    Intents,
)
from discord.ext import commands
from discord.ext.commands import Context
import yt_dlp

intents = Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix="/", intents=intents)
audio_path = "./audio"
status_channel_name = "member-status"
max_audio_length = 10


@bot.event
async def on_ready() -> None:
    print("We have logged in as {0.user}".format(bot))


def match_target_amplitude(sound, target_dBFS):
    change_in_dBFS = target_dBFS - sound.dBFS
    return sound.apply_gain(change_in_dBFS)


def download_youtube(member: Member, url: str, start_time: float = 0, end_time: float = None) -> None:
    """
    Downloads a portion of a YouTube video's audio stream as an MP3 file and saves it to disk.

    Args:
        member (discord.Member): The member who requested the download.
        url (str): The URL of the YouTube video to download.
        start_time (float, optional): The start time of the audio segment to download, in seconds. Defaults to 0.
        end_time (float, optional): The end time of the audio segment to download, in seconds. If None, defaults to 10
            seconds after the start time.

    Raises:
        Exception: If the requested audio segment is longer than the maximum allowed length and the requesting
            member is not authorized to download longer files.

    Returns:
        None
    """

    def my_hook(d):  # pragma: no cover
        if d["status"] == "finished":
            print("Done downloading, now converting ...")

    start_time = float(start_time)
    end_time = float(end_time) if end_time else start_time + 10

    requested_length = end_time - start_time
    if member.display_name != "KnickJ" and requested_length > max_audio_length:
        raise Exception(f"Audio is {requested_length}s long, maximum is {max_audio_length}s.")

    file = f"{audio_path}/{member.id}.mp3"
    if not os.path.exists(audio_path):
        os.makedirs(audio_path)
    ydl_opts = {
        "format": "bestaudio/best",
        "outtmpl": file,
        "progress_hooks": [my_hook],
        "download_ranges": lambda *_: [{"start_time": start_time, "end_time": end_time}],
        "force_keyframes_at_cuts": True,
    }
    if os.path.exists(file):
        os.remove(file)
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        ydl.download([url])

    # Apply volume to downloaded file
    sound = AudioSegment.from_file(file)
    sound = match_target_amplitude(sound, -20.0)
    sound.export(file, format="mp3")

    print("Finished downloading")


def download_file(member, url, start_time: float = 0, end_time: float = None) -> None:
    """
    Downloads an audio file from a given URL and saves it to disk, compressing it if necessary.

    Args:
        member (discord.Member): The member who requested the file.
        url (str): The URL of the file to download.
        start_time (float, optional): The start time of the audio segment to download, in seconds. Defaults to None.
        end_time (float, optional): The end time of the audio segment to download, in seconds. Defaults to None.

    Raises:
        ValueError: If the file type is not supported or if the file exceeds the maximum length.

    Returns:
        None
    """
    start_time = float(start_time)
    end_time = float(end_time) if end_time else start_time + 10

    _, ext = os.path.splitext(url)
    if ext.lower() not in [".mp3", ".wav", ".ogg"]:
        raise ValueError("File type not supported. Supported types are .mp3, .wav, and .ogg")

    headers = {"User-Agent": "Mozilla/5.0"}
    response = requests.get(url, headers=headers)

    if response.status_code != 200:
        raise Exception(f"Failed to download file from {url}")

    if not os.path.exists(audio_path):
        os.makedirs(audio_path)

    file_path = f"{audio_path}/{member.id}.mp3"
    with open(file_path, "wb") as f:
        f.write(response.content)

    # Load the downloaded file and trim it to the specified start and end times, if given
    sound = AudioSegment.from_file(file_path, format=ext[1:])
    if start_time is not None:
        start_time = start_time * 1000  # convert to milliseconds
    if end_time is not None:
        end_time = end_time * 1000  # convert to milliseconds
    sound = sound[int(start_time) : int(end_time)]
    sound = match_target_amplitude(sound, -20.0)

    sound.export(file_path, format="mp3", bitrate="320k", parameters=["-q:a", "0"])

    print(f"File downloaded and compressed: {file_path}")


def get_seconds(time_str: str) -> float:
    """
    Convert a time string in the format "mm:ss" to seconds as a float.

    Args:
        time_str (str): The time string to convert.

    Returns:
        float: The total number of seconds as a float.
    """
    try:
        minutes, seconds = map(float, time_str.split(":"))
        return minutes * 60 + seconds
    except (ValueError, AttributeError):
        return float(time_str)


def download_music(member: Member, url: str, start: Optional[str] = None, end: Optional[str] = None) -> None:
    """
    Downloads music from a given URL and saves it to disk, compressing it if necessary.

    Args:
        member (discord.Member): The member who requested the music.
        url (str): The URL of the music to download.
        start (Optional[str]): The start time of the music to download, formatted as "mm:ss".
        end (Optional[str]): The end time of the music to download, formatted as "mm:ss".

    Raises:
        ValueError: If the file type is not supported or if the file exceeds the maximum length.

    Returns:
        None
    """
    if "youtube" in url or "youtu.be" in url:
        download_youtube(member, url, get_seconds(start) if start else 0, get_seconds(end) if end else None)
    else:
        download_file(member, url, get_seconds(start) if start else 0, get_seconds(end) if end else None)


@bot.command(name="set-sound")
async def set_walk_on(ctx: Context, *args) -> None:
    """
    Discord bot command to set the sound for a given user.

    Args:
        ctx (Context): The Discord context of the command.
        *args (List[str]): The command arguments, expected to be a URL to a music file.

    Returns:
        None

    """
    member: Member = ctx.author
    print(f"Member: {member}, Message: /set-sound {args}")
    try:
        download_music(member, *args)
    except Exception as e:
        traceback.print_exception(e)
        await ctx.send(f"Error while setting sound for {member.display_name}.\n```css\n[{e}]\n```")
        return
    await ctx.send(f":notes: Set sound for {member.display_name} :notes:")


@bot.command(name="get-sound")
async def get_walk_on(ctx: Context) -> None:
    """
    Discord bot command to get the sound file for a given user.

    Args:
        ctx (Context): The Discord context of the command.

    Returns:
        None

    """
    member: Member = ctx.author
    print(f"Member: {member}, Message: /get-sound")
    filename = glob(f"{audio_path}/{member.id}.*")
    ext = filename[0].split(".")[-1]
    send_file = File(filename[0], filename=f"{member.display_name}.{ext}")
    await ctx.send(file=send_file)


async def send_usage(ctx: Context) -> None:
    usage = """usage:
        /whoson set {url} [start_time] [end_time]: set whoson sound for the current user
            url: accepts direct audio link or youtube video link, youtube video is trimmed to 10s
            start_time: (youtube only, optional) set the start time of the sound in seconds
            end_time: (youtube only, optional) set the end time of the sound in seconds, maximum of start_time + 10
        /whoson get : get current whoson sound and post to text channel
        /whoson play : play current whoson sound in voice channel"""
    await ctx.send(usage)


@bot.command(name="whoson")
async def walk_on_cli(ctx: Context, *args) -> None:
    """Command to manage and display information about users "walk on" music.

    Args:
        ctx (Context): The context of the command.
        args (Tuple[str]): The command arguments.

    Returns:
        None: This function returns nothing, it's just updating some information.

    Raises:
        None: This function does not raise any exceptions.
    """
    member: Member = ctx.author
    print(f"Member: {member}, Message: /whoson {args}")
    if len(args) <= 0:
        await send_usage(ctx)
        return
    match args[0]:
        case "help":
            await send_usage(ctx)
        case "set":
            await set_walk_on(ctx, *args[1:])
            await get_walk_on(ctx)
        case "get":
            await get_walk_on(ctx)
        case "play":
            if member and member.voice:
                await play_channel_sound(member, member.voice.channel)


def get_guild_channel_by_name(guild: Guild, channel_name: str) -> TextChannel | None:
    """Get a guild channel by name.

    Args:
        guild (Guild): The guild where to search the channel.
        channel_name (str): The name of the channel to search for.

    Returns:
        Union[TextChannel, None]: The TextChannel if found, None otherwise.

    Raises:
        None: This function does not raise any exceptions.
    """
    for channel in guild.text_channels:
        if channel.name == channel_name:
            return channel
    return None


async def update_member_status(member: Member, voice_state: VoiceState) -> None:
    """Update the member status on a given voice channel.

    Args:
        member (Member): The member whose status will be updated.
        voice_state (VoiceState): The new voice state.

    Returns:
        None: This function returns nothing, it's just updating some information.

    Raises:
        None: This function does not raise any exceptions.
    """
    voice_channel = voice_state.channel
    if voice_channel:
        status_channel = get_guild_channel_by_name(voice_channel.guild, status_channel_name)
        if status_channel:
            await status_channel.send(f"{member.display_name} is online")


@bot.event
async def on_voice_state_update(member: Member, before: VoiceState, after: VoiceState) -> None:
    """
    Handles the bot behavior when a member's voice state changes.

    Args:
        member (discord.Member): The member whose voice state changed.
        before (discord.VoiceState): The previous voice state of the member.
        after (discord.VoiceState): The new voice state of the member.

    Returns:
        None.
    """
    if member == bot.user:
        print("Bot State Change")
        return

    if after.channel is not None and before.channel is None:
        await update_member_status(member, after)

    if after.channel is not None and after.channel is not before.channel:
        await play_channel_sound(member, after.channel)


async def play_channel_sound(member: Member, channel: Union[VoiceChannel, StageChannel, None]):
    """
    Plays the sound associated with a member in the voice channel.

    Args:
        member (discord.Member): The member that joined the channel.
        channel (discord.VocalGuildChannel): The voice channel that the member joined.

    Returns:
        None.
    """
    filename = glob(f"{audio_path}/{member.id}.*")
    if len(filename) > 0:
        voice: VoiceClient = await channel.connect(cls=VoiceClient)  # type: ignore
        source = FFmpegPCMAudio(filename[0])
        await asyncio.sleep(0.1)
        voice.play(source)
        while True:
            await asyncio.sleep(0.5)
            if not voice.is_playing():
                await voice.disconnect()
                break


if __name__ == "__main__":  # pragma: no cover
    import config

    bot.run(config.auth_key)
