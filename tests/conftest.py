from unittest.mock import MagicMock

import pytest
from discord import Guild, TextChannel, VoiceChannel, Member


@pytest.fixture(scope="function")
def mock_guild() -> Guild:
    guild = Guild(data={"id": 1}, state=MagicMock())
    member_status_channel = MagicMock(spec=TextChannel)
    member_status_channel.name = "member-status"
    voice_channel = MagicMock(spec=VoiceChannel, guild=guild)
    voice_channel.name = "voice-channel"
    guild._channels = {"member-status": member_status_channel, "voice-channel": voice_channel}
    return guild


@pytest.fixture(scope="function")
def mock_member() -> Member:
    member = MagicMock(spec=Member, display_name="TestUser", id=1)
    return member


@pytest.fixture
def mock_youtube_download(mocker):
    yield mocker.patch("yt_dlp.YoutubeDL", MagicMock())


@pytest.fixture(autouse=True)
def mock_makedirs(mocker):
    yield mocker.patch("os.makedirs")


@pytest.fixture(autouse=True)
def mock_remove(mocker):
    yield mocker.patch("os.remove")


@pytest.fixture
def mock_audio_segment(mocker):
    mock_audio_segment = MagicMock()
    mocker.patch("pydub.AudioSegment.from_file", return_value=mock_audio_segment)
    mock_audio_segment.__getitem__.return_value = mock_audio_segment
    mock_audio_segment.apply_gain.return_value = mock_audio_segment

    return mock_audio_segment